module com.kelompok7 {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires org.controlsfx.controls;

    exports com.kelompok7;

    opens com.kelompok7 to javafx.fxml;

    exports com.kelompok7.controller.mainFX;

    opens com.kelompok7.controller.mainFX to javafx.fxml;
}
