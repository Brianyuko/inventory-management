package com.kelompok7.model.drop_out;

public class DropOut {

    private int dropOutId;
    private int productId;
    private String productName;
    private int quantity;
    private String createdAt;

    public DropOut(int dropOutId, int productId, int quantity, String createdAt) {
        this.dropOutId = dropOutId;
        this.productId = productId;
        this.quantity = quantity;
        this.createdAt = createdAt;
    }

    public DropOut(int dropOutId, int productId, int quantity, String createdAt, String productName) {
        this.dropOutId = dropOutId;
        this.productId = productId;
        this.quantity = quantity;
        this.createdAt = createdAt;
        this.productName = productName;

    }

    public DropOut(int productId, int quantity) {
        this.productId = productId;
        this.quantity = quantity;
    }

    public int getDropOutId() {
        return dropOutId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public void setDropOutId(int dropOutId) {
        this.dropOutId = dropOutId;
    }

}
