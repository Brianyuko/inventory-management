package com.kelompok7.model.drop_in;

public class DropIn {
    private int dropInId;
    private int productId;
    private String productName;
    private int quantity;
    private String createdAt;

    public DropIn(int dropInId, int productId, int quantity, String createdAt) {
        this.dropInId = dropInId;
        this.productId = productId;
        this.quantity = quantity;
        this.createdAt = createdAt;
    }

    public DropIn(int dropInId, int productId, int quantity, String createdAt, String productName) {
        this.dropInId = dropInId;
        this.productId = productId;
        this.quantity = quantity;
        this.createdAt = createdAt;
        this.productName = productName;

    }

    public DropIn(int productId, int quantity) {
        this.productId = productId;
        this.quantity = quantity;
    }

    public int getDropInId() {
        return dropInId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public void setDropInId(int dropInId) {
        this.dropInId = dropInId;
    }

}
