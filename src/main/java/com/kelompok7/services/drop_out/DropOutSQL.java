package com.kelompok7.services.drop_out;

import java.sql.*;

import com.kelompok7.model.drop_out.DropOut;

public class DropOutSQL {
    private Connection con;
    private ResultSet result = null;

    public DropOutSQL() {
        try {
            String nameDB = "inventory-app";
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + nameDB, "root", "");
        } catch (Exception e) {
            System.err.println("ERROR - SQL: " + e);
        }
    }

    public ResultSet getData() {
        try {
            Statement stmt = con.createStatement();
            result = stmt.executeQuery(
                    "select drop_out_id, product_id, quantity_out, created_at, product_name from drop_out inner join product on product.product_id = drop_out.fk_product_id;");
        } catch (Exception e) {
            System.err.println("ERROR - SQL: " + e);
        }
        return result;
    }

    // CREATE
    public void insertData(DropOut dropin) {
        try {
            String query = " insert into drop_out (fk_product_id, quantity_out) values (?,?)";
            // String sql = "UPDATE " + TABLE_FLIGHTS + " SET " + COLUMN_FLIGHTS_QUANTITY +
            // " = " + COLUMN_FLIGHTS_QUANTITY
            // + " - 1 WHERE " + COLUMN_FLIGHTS_ID + " = ?";
            String queryDecrease = " update product set quantity=quantity-?  where product_id=?;";
            PreparedStatement preparedStmt = con.prepareStatement(query);
            PreparedStatement preparedStmtDecrease = con.prepareStatement(queryDecrease);
            preparedStmt.setInt(1, dropin.getProductId());
            preparedStmt.setInt(2, dropin.getQuantity());
            preparedStmtDecrease.setInt(1, dropin.getQuantity());
            preparedStmtDecrease.setInt(2, dropin.getProductId());
            preparedStmt.execute();
            preparedStmtDecrease.execute();
        } catch (Exception e) {
            System.err.println("ERROR - SQL: " + e);
        }
    }

    public void closeSQL() {
        try {
            con.close();
        } catch (Exception e) {
            System.err.println("ERROR - SQL: " + e);
        }
    }
}
