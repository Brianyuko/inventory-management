package com.kelompok7.services.product;

import com.kelompok7.model.product.Product;
import java.sql.*;

public class ProductSQL {

    private Connection con;
    private ResultSet result = null;

    public ProductSQL() {
        try {
            String nameDB = "inventory-app";
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + nameDB, "root", "");
        } catch (Exception e) {
            System.err.println("ERROR - SQL: " + e);
        }
    }

    // READ
    public ResultSet getData() {
        try {
            Statement stmt = con.createStatement();
            result = stmt.executeQuery(
                    "select product_id, product_name, product_description, category_id, quantity, category_name from product inner join category on category.category_id = product.fk_category_id;");
        } catch (Exception e) {
            System.err.println("ERROR - SQL: " + e);
        }
        return result;
    }

    // CREATE
    public void insertData(Product product) {
        try {
            String query = " insert into product (product_name, product_description, fk_category_id, quantity) values (?,?,?,?)";
            PreparedStatement preparedStmt = con.prepareStatement(query);
            preparedStmt.setString(1, product.getProductName());
            preparedStmt.setString(2, product.getProductDescription());
            preparedStmt.setInt(3, product.getCategoryId());
            preparedStmt.setInt(4, 0);
            preparedStmt.execute();
        } catch (Exception e) {
            System.err.println("ERROR - SQL: " + e);
        }
    }

    // UPDATE
    public void updateData(Product product) {
        try {
            String query = " update product set product_name=?, product_description=?,fk_category_id=?  where product_id=?;";
            PreparedStatement preparedStmt = con.prepareStatement(query);
            preparedStmt.setString(1, product.getProductName());
            preparedStmt.setString(2, product.getProductDescription());
            preparedStmt.setInt(3, product.getCategoryId());
            preparedStmt.setInt(4, product.getProductId());
            preparedStmt.execute();
        } catch (Exception e) {
            System.err.println("ERROR - SQL: " + e);
        }
    }

    // Increase Stock
    public void increaseStock(Product product) {
        try {
            String query = " update product set product_name=?, product_description=?,fk_category_id=?  where product_id=?;";
            PreparedStatement preparedStmt = con.prepareStatement(query);
            preparedStmt.setString(1, product.getProductName());
            preparedStmt.setString(2, product.getProductDescription());
            preparedStmt.setInt(3, product.getCategoryId());
            preparedStmt.setInt(4, product.getProductId());
            preparedStmt.execute();
        } catch (Exception e) {
            System.err.println("ERROR - SQL: " + e);
        }
    }

    // DELETE
    public void deleteData(Product product) {
        try {
            String query = " delete from product where product_id=?";
            PreparedStatement preparedStmt = con.prepareStatement(query);
            preparedStmt.setInt(1, product.getProductId());
            preparedStmt.execute();
        } catch (Exception e) {
            System.err.println("ERROR - SQL: " + e);
        }
    }

    public void closeSQL() {
        try {
            con.close();
        } catch (Exception e) {
            System.err.println("ERROR - SQL: " + e);
        }
    }

}