package com.kelompok7.services.drop_in;

import java.sql.*;

import com.kelompok7.model.drop_in.DropIn;

public class DropInSQL {
    private Connection con;
    private ResultSet result = null;

    public DropInSQL() {
        try {
            String nameDB = "inventory-app";
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + nameDB, "root", "");
        } catch (Exception e) {
            System.err.println("ERROR - SQL: " + e);
        }
    }

    public ResultSet getData() {
        try {
            Statement stmt = con.createStatement();
            result = stmt.executeQuery(
                    "select drop_in_id, product_id, quantity_in, created_at, product_name from drop_in inner join product on product.product_id = drop_in.fk_product_id;");
        } catch (Exception e) {
            System.err.println("ERROR - SQL: " + e);
        }
        return result;
    }

    // CREATE
    public void insertData(DropIn dropin) {
        try {
            String query = " insert into drop_in (fk_product_id, quantity_in) values (?,?)";
            // String sql = "UPDATE " + TABLE_FLIGHTS + " SET " + COLUMN_FLIGHTS_QUANTITY +
            // " = " + COLUMN_FLIGHTS_QUANTITY
            // + " - 1 WHERE " + COLUMN_FLIGHTS_ID + " = ?";
            String queryIncrease = " update product set quantity=quantity+?  where product_id=?;";
            PreparedStatement preparedStmt = con.prepareStatement(query);
            PreparedStatement preparedStmtIncrease = con.prepareStatement(queryIncrease);
            preparedStmt.setInt(1, dropin.getProductId());
            preparedStmt.setInt(2, dropin.getQuantity());
            preparedStmtIncrease.setInt(1, dropin.getQuantity());
            preparedStmtIncrease.setInt(2, dropin.getProductId());
            preparedStmt.execute();
            preparedStmtIncrease.execute();
        } catch (Exception e) {
            System.err.println("ERROR - SQL: " + e);
        }
    }

    public void closeSQL() {
        try {
            con.close();
        } catch (Exception e) {
            System.err.println("ERROR - SQL: " + e);
        }
    }
}
