package com.kelompok7.services.user;

import com.kelompok7.model.category.Category;
import com.kelompok7.model.user.User;

import java.sql.*;

public class UserSQL {

    private Connection con;
    private ResultSet result = null;

    public UserSQL() {
        try {
            String nameDB = "inventory-app";
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + nameDB, "root", "");
        } catch (Exception e) {
            System.err.println("ERROR - SQL: " + e);
        }
    }

    // READ
    public ResultSet getData() {
        try {
            Statement stmt = con.createStatement();
            result = stmt.executeQuery("select * from users");
        } catch (Exception e) {
            System.err.println("ERROR - SQL: " + e);
        }
        return result;
    }

    // CREATE
    public void insertData(User user) {
        try {
            String query = " insert into users (username, password) values (?,?)";
            PreparedStatement preparedStmt = con.prepareStatement(query);
            preparedStmt.setString(1, user.getUsername());
            preparedStmt.setString(2, user.getPassword());
            preparedStmt.execute();
        } catch (Exception e) {
            System.err.println("ERROR - SQL: " + e);
        }
    }

    // UPDATE
    public void updateData(Category category) {
        try {
            String query = " update category set category_name=? where category_id=?;";
            PreparedStatement preparedStmt = con.prepareStatement(query);
            preparedStmt.setString(1, category.getCategoryName());
            preparedStmt.setInt(2, category.getCategoryId());
            preparedStmt.execute();
        } catch (Exception e) {
            System.err.println("ERROR - SQL: " + e);
        }
    }

    public void closeSQL() {
        try {
            con.close();
        } catch (Exception e) {
            System.err.println("ERROR - SQL: " + e);
        }
    }

}
