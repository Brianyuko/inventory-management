package com.kelompok7.controller.drop_out;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.kelompok7.model.drop_out.DropOut;
import com.kelompok7.services.drop_out.DropOutSQL;

public class DropOutController {
    public List<DropOut> readDropOut() {
        List<DropOut> list = new ArrayList<>();
        try {
            DropOutSQL dropinSQL = new DropOutSQL();
            ResultSet result = dropinSQL.getData();
            while (result.next()) {
                DropOut dropin = new DropOut(result.getInt(1), result.getInt(2), result.getInt(3),
                        result.getString(4), result.getString(5));
                list.add(dropin);
            }
            dropinSQL.closeSQL();
        } catch (Exception e) {
            System.err.println("ERROR - CONTROLLER:" + e);
        }
        return list;
    }

    public void createDropOut(int product_id, int quantity) {
        try {
            DropOut dropin = new DropOut(product_id, quantity);

            DropOutSQL dropinSQL = new DropOutSQL();
            dropinSQL.insertData(dropin);

            dropinSQL.closeSQL();
        } catch (Exception e) {
            System.err.println("ERROR - CONTROLLER:" + e);
        }
    }
}
