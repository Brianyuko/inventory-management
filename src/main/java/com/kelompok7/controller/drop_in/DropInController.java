package com.kelompok7.controller.drop_in;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.kelompok7.model.drop_in.DropIn;
import com.kelompok7.services.drop_in.DropInSQL;

public class DropInController {
    public List<DropIn> readDropIn() {
        List<DropIn> list = new ArrayList<>();
        try {
            DropInSQL dropinSQL = new DropInSQL();
            ResultSet result = dropinSQL.getData();
            while (result.next()) {
                DropIn dropin = new DropIn(result.getInt(1), result.getInt(2), result.getInt(3),
                        result.getString(4), result.getString(5));
                list.add(dropin);
            }
            dropinSQL.closeSQL();
        } catch (Exception e) {
            System.err.println("ERROR - CONTROLLER:" + e);
        }
        return list;
    }

    public void createDropIn(int product_id, int quantity) {
        try {
            DropIn dropin = new DropIn(product_id, quantity);

            DropInSQL dropinSQL = new DropInSQL();
            dropinSQL.insertData(dropin);

            dropinSQL.closeSQL();
        } catch (Exception e) {
            System.err.println("ERROR - CONTROLLER:" + e);
        }
    }
}
