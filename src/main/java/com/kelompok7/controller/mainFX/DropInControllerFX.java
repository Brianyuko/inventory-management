package com.kelompok7.controller.mainFX;

import java.util.List;

import com.kelompok7.controller.drop_in.DropInController;
import com.kelompok7.controller.product.ProductController;
import com.kelompok7.model.drop_in.DropIn;
import com.kelompok7.model.product.Product;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

public class DropInControllerFX {

    @FXML
    public TextField textFieldCreateDropInQuantity;
    public TextField textFieldUpdateProductId;
    public TextField textFieldUpdateDropInQuantity;

    @FXML
    public ComboBox<Product> comboBoxCreateDropInProduct;

    @FXML
    public TableView<DropIn> tableDropIn;
    public TableColumn<DropIn, String> tableColumnDropInId;
    public TableColumn<DropIn, String> tableColumnDropInProduct;
    public TableColumn<DropIn, String> tableColumnDropInQuantity;
    public TableColumn<DropIn, String> tableColumnDropInCreatedAt;

    @FXML
    public void initialize() {
        tableSetUp();
    }

    private void tableSetUp() {
        DropInController dropinController = new DropInController();
        ProductController productController = new ProductController();

        List<DropIn> listDropIn = dropinController.readDropIn();
        List<Product> listProduct = productController.readProduct();

        tableDropIn.setItems(FXCollections.observableArrayList(listDropIn));

        // comboBoxCreateProductCategory.getItems().addAll(listCategory);
        comboBoxCreateDropInProduct.setItems(FXCollections.observableArrayList(listProduct));
        comboBoxCreateDropInProduct.setValue((listProduct.size() == 0) ? null : (Product) listProduct.get(2));

        tableColumnDropInId.setCellValueFactory(
                dropin -> new SimpleStringProperty(String.valueOf(dropin.getValue().getDropInId())));
        tableColumnDropInProduct.setCellValueFactory(
                dropin -> new SimpleStringProperty(String.valueOf(dropin.getValue().getProductName())));
        tableColumnDropInQuantity.setCellValueFactory(
                dropin -> new SimpleStringProperty(String.valueOf(dropin.getValue().getQuantity())));
        tableColumnDropInCreatedAt
                .setCellValueFactory(dropin -> new SimpleStringProperty(dropin.getValue().getCreatedAt()));
    }

    // Refresh Read Product
    public void onButtonClickedRefreshDropIn() {
        DropInController dropinController = new DropInController();
        List<DropIn> list = dropinController.readDropIn();
        tableDropIn.setItems(FXCollections.observableArrayList(list));
    }

    // Create Product
    public void onButtonClickedCreateDropIn() {
        DropInController dropinController = new DropInController();
        Product product_id = comboBoxCreateDropInProduct.getValue();
        dropinController.createDropIn(product_id.getProductId(),
                Integer.parseInt(textFieldCreateDropInQuantity.getText()));
        textFieldCreateDropInQuantity.setText("");
        onButtonClickedRefreshDropIn();
    }
}
