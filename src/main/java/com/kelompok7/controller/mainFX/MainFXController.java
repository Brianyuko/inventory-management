package com.kelompok7.controller.mainFX;

import com.kelompok7.App;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;

import java.io.IOException;

public class MainFXController {

    @FXML
    public Pane currentPane;

    @FXML
    public void initialize() {
        buttonChangeProductPane();

    }

    // Change to Product Window
    public void buttonChangeProductPane() {
        Pane newLoadedPane = null;
        try {
            newLoadedPane = FXMLLoader.load(App.class.getResource("paneProduct.fxml"));
            currentPane.getChildren().setAll(newLoadedPane);
        } catch (IOException e) {
            System.out.println("ERROR PANE:" + e);
        }

    }

    // Change to Drop In Window
    public void buttonChangeDropInPane() {
        Pane newLoadedPane = null;
        try {
            newLoadedPane = FXMLLoader.load(App.class.getResource("paneDropIn.fxml"));
            currentPane.getChildren().setAll(newLoadedPane);
        } catch (IOException e) {
            System.out.println("ERROR PANE:" + e);
        }

    }

    // Change to Drop Out Window
    public void buttonChangeDropOutPane() {
        Pane newLoadedPane = null;
        try {
            newLoadedPane = FXMLLoader.load(App.class.getResource("paneDropOut.fxml"));
            currentPane.getChildren().setAll(newLoadedPane);
        } catch (IOException e) {
            System.out.println("ERROR PANE:" + e);
        }

    }

    // Change to Log In Window
    public void buttonChangeLoginScreen() {
        Pane newLoadedPane = null;
        try {
            newLoadedPane = FXMLLoader.load(App.class.getResource("LoginView.fxml"));
            currentPane.getChildren().setAll(newLoadedPane);
        } catch (IOException e) {
            System.out.println("ERROR PANE:" + e);
        }

    }

    // Change to Category Window
    public void buttonChangeCategoryPane() {
        Pane newLoadedPane = null;
        try {
            newLoadedPane = FXMLLoader.load(App.class.getResource("paneCategory.fxml"));
            currentPane.getChildren().setAll(newLoadedPane);
        } catch (IOException e) {
            System.out.println("ERROR PANE:" + e);
        }

    }

    // Change to Category Order
    public void buttonChangeCreateOrder() {
        Pane newLoadedPane = null;
        try {
            newLoadedPane = FXMLLoader.load(App.class.getResource("paneOrderCart.fxml"));
            currentPane.getChildren().setAll(newLoadedPane);
        } catch (IOException e) {
            System.out.println("ERROR PANE:" + e);
        }

    }

    public void buttonChangeViewOrder() {
        Pane newLoadedPane = null;
        try {
            newLoadedPane = FXMLLoader.load(App.class.getResource("paneOrders.fxml"));
            currentPane.getChildren().setAll(newLoadedPane);
        } catch (IOException e) {
            System.out.println("ERROR PANE:" + e);
        }
    }
}
