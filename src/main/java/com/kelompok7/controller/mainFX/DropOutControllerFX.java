package com.kelompok7.controller.mainFX;

import java.util.List;

import com.kelompok7.controller.drop_out.DropOutController;
import com.kelompok7.controller.product.ProductController;
import com.kelompok7.model.drop_out.DropOut;
import com.kelompok7.model.product.Product;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

public class DropOutControllerFX {

    @FXML
    public TextField textFieldCreateDropOutQuantity;
    public TextField textFieldUpdateProductId;
    public TextField textFieldUpdateDropOutQuantity;

    @FXML
    public ComboBox<Product> comboBoxCreateDropOutProduct;

    @FXML
    public TableView<DropOut> tableDropOut;
    public TableColumn<DropOut, String> tableColumnDropOutId;
    public TableColumn<DropOut, String> tableColumnDropOutProduct;
    public TableColumn<DropOut, String> tableColumnDropOutQuantity;
    public TableColumn<DropOut, String> tableColumnDropOutCreatedAt;

    @FXML
    public void initialize() {
        tableSetUp();
    }

    private void tableSetUp() {
        DropOutController dropoutController = new DropOutController();
        ProductController productController = new ProductController();

        List<DropOut> listDropOut = dropoutController.readDropOut();
        List<Product> listProduct = productController.readProduct();

        tableDropOut.setItems(FXCollections.observableArrayList(listDropOut));

        // comboBoxCreateProductCategory.getItems().addAll(listCategory);
        comboBoxCreateDropOutProduct.setItems(FXCollections.observableArrayList(listProduct));
        comboBoxCreateDropOutProduct.setValue((listProduct.size() == 0) ? null : (Product) listProduct.get(2));

        tableColumnDropOutId.setCellValueFactory(
                dropout -> new SimpleStringProperty(String.valueOf(dropout.getValue().getDropOutId())));
        tableColumnDropOutProduct.setCellValueFactory(
                dropout -> new SimpleStringProperty(String.valueOf(dropout.getValue().getProductName())));
        tableColumnDropOutQuantity.setCellValueFactory(
                dropout -> new SimpleStringProperty(String.valueOf(dropout.getValue().getQuantity())));
        tableColumnDropOutCreatedAt
                .setCellValueFactory(dropout -> new SimpleStringProperty(dropout.getValue().getCreatedAt()));
    }

    // Refresh Read Product
    public void onButtonClickedRefreshDropOut() {
        DropOutController dropoutController = new DropOutController();
        List<DropOut> list = dropoutController.readDropOut();
        tableDropOut.setItems(FXCollections.observableArrayList(list));
    }

    // Create Product
    public void onButtonClickedCreateDropOut() {
        DropOutController dropoutController = new DropOutController();
        Product product_id = comboBoxCreateDropOutProduct.getValue();
        dropoutController.createDropOut(product_id.getProductId(),
                Integer.parseInt(textFieldCreateDropOutQuantity.getText()));
        textFieldCreateDropOutQuantity.setText("");
        onButtonClickedRefreshDropOut();
    }
}
